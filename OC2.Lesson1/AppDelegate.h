//
//  AppDelegate.h
//  OC2.Lesson1
//
//  Created by Igor Gryshakin on 1/29/17.
//  Copyright © 2017 Grykin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MarqueeLabel.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

